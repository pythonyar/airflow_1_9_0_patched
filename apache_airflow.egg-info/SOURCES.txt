CHANGELOG.txt
DISCLAIMER
LICENSE
MANIFEST.in
NOTICE
README.md
setup.cfg
setup.py
airflow/__init__.py
airflow/alembic.ini
airflow/configuration.py
airflow/default_login.py
airflow/exceptions.py
airflow/jobs.py
airflow/logging_config.py
airflow/minihivecluster.py
airflow/models.py
airflow/plugins_manager.py
airflow/settings.py
airflow/version.py
airflow/api/__init__.py
airflow/api/auth/__init__.py
airflow/api/auth/backend/__init__.py
airflow/api/auth/backend/default.py
airflow/api/auth/backend/deny_all.py
airflow/api/auth/backend/kerberos_auth.py
airflow/api/client/__init__.py
airflow/api/client/api_client.py
airflow/api/client/json_client.py
airflow/api/client/local_client.py
airflow/api/common/__init__.py
airflow/api/common/experimental/__init__.py
airflow/api/common/experimental/get_task.py
airflow/api/common/experimental/get_task_instance.py
airflow/api/common/experimental/mark_tasks.py
airflow/api/common/experimental/pool.py
airflow/api/common/experimental/trigger_dag.py
airflow/bin/__init__.py
airflow/bin/airflow
airflow/bin/cli.py
airflow/config_templates/__init__.py
airflow/config_templates/__init__.pyc
airflow/config_templates/airflow_local_settings.py
airflow/config_templates/airflow_local_settings.pyc
airflow/config_templates/default_airflow.cfg
airflow/config_templates/default_celery.py
airflow/config_templates/default_test.cfg
airflow/contrib/__init__.py
airflow/contrib/auth/__init__.py
airflow/contrib/auth/backends/__init__.py
airflow/contrib/auth/backends/github_enterprise_auth.py
airflow/contrib/auth/backends/google_auth.py
airflow/contrib/auth/backends/kerberos_auth.py
airflow/contrib/auth/backends/ldap_auth.py
airflow/contrib/auth/backends/password_auth.py
airflow/contrib/executors/__init__.py
airflow/contrib/executors/mesos_executor.py
airflow/contrib/hooks/__init__.py
airflow/contrib/hooks/aws_dynamodb_hook.py
airflow/contrib/hooks/aws_hook.py
airflow/contrib/hooks/aws_lambda_hook.py
airflow/contrib/hooks/bigquery_hook.py
airflow/contrib/hooks/cloudant_hook.py
airflow/contrib/hooks/databricks_hook.py
airflow/contrib/hooks/datadog_hook.py
airflow/contrib/hooks/datastore_hook.py
airflow/contrib/hooks/emr_hook.py
airflow/contrib/hooks/fs_hook.py
airflow/contrib/hooks/ftp_hook.py
airflow/contrib/hooks/gcp_api_base_hook.py
airflow/contrib/hooks/gcp_dataflow_hook.py
airflow/contrib/hooks/gcp_dataproc_hook.py
airflow/contrib/hooks/gcp_mlengine_hook.py
airflow/contrib/hooks/gcp_pubsub_hook.py
airflow/contrib/hooks/gcs_hook.py
airflow/contrib/hooks/jira_hook.py
airflow/contrib/hooks/qubole_hook.py
airflow/contrib/hooks/redis_hook.py
airflow/contrib/hooks/redshift_hook.py
airflow/contrib/hooks/salesforce_hook.py
airflow/contrib/hooks/spark_sql_hook.py
airflow/contrib/hooks/spark_submit_hook.py
airflow/contrib/hooks/sqoop_hook.py
airflow/contrib/hooks/ssh_hook.py
airflow/contrib/hooks/vertica_hook.py
airflow/contrib/hooks/wasb_hook.py
airflow/contrib/operators/__init__.py
airflow/contrib/operators/bigquery_check_operator.py
airflow/contrib/operators/bigquery_operator.py
airflow/contrib/operators/bigquery_table_delete_operator.py
airflow/contrib/operators/bigquery_to_bigquery.py
airflow/contrib/operators/bigquery_to_gcs.py
airflow/contrib/operators/databricks_operator.py
airflow/contrib/operators/dataflow_operator.py
airflow/contrib/operators/dataproc_operator.py
airflow/contrib/operators/datastore_export_operator.py
airflow/contrib/operators/datastore_import_operator.py
airflow/contrib/operators/druid_operator.py
airflow/contrib/operators/ecs_operator.py
airflow/contrib/operators/emr_add_steps_operator.py
airflow/contrib/operators/emr_create_job_flow_operator.py
airflow/contrib/operators/emr_terminate_job_flow_operator.py
airflow/contrib/operators/file_to_gcs.py
airflow/contrib/operators/file_to_wasb.py
airflow/contrib/operators/fs_operator.py
airflow/contrib/operators/gcs_download_operator.py
airflow/contrib/operators/gcs_to_bq.py
airflow/contrib/operators/hipchat_operator.py
airflow/contrib/operators/hive_to_dynamodb.py
airflow/contrib/operators/jira_operator.py
airflow/contrib/operators/mlengine_operator.py
airflow/contrib/operators/mlengine_operator_utils.py
airflow/contrib/operators/mlengine_prediction_summary.py
airflow/contrib/operators/mysql_to_gcs.py
airflow/contrib/operators/pubsub_operator.py
airflow/contrib/operators/qubole_operator.py
airflow/contrib/operators/sftp_operator.py
airflow/contrib/operators/spark_sql_operator.py
airflow/contrib/operators/spark_submit_operator.py
airflow/contrib/operators/sqoop_operator.py
airflow/contrib/operators/ssh_operator.py
airflow/contrib/operators/vertica_operator.py
airflow/contrib/operators/vertica_to_hive.py
airflow/contrib/sensors/__init__.py
airflow/contrib/sensors/bigquery_sensor.py
airflow/contrib/sensors/datadog_sensor.py
airflow/contrib/sensors/emr_base_sensor.py
airflow/contrib/sensors/emr_job_flow_sensor.py
airflow/contrib/sensors/emr_step_sensor.py
airflow/contrib/sensors/ftp_sensor.py
airflow/contrib/sensors/gcs_sensor.py
airflow/contrib/sensors/hdfs_sensors.py
airflow/contrib/sensors/jira_sensor.py
airflow/contrib/sensors/redis_key_sensor.py
airflow/contrib/sensors/wasb_sensor.py
airflow/contrib/task_runner/__init__.py
airflow/contrib/task_runner/cgroup_task_runner.py
airflow/dag/__init__.py
airflow/dag/base_dag.py
airflow/example_dags/__init__.py
airflow/example_dags/docker_copy_data.py
airflow/example_dags/example_bash_operator.py
airflow/example_dags/example_branch_operator.py
airflow/example_dags/example_branch_python_dop_operator_3.py
airflow/example_dags/example_docker_operator.py
airflow/example_dags/example_http_operator.py
airflow/example_dags/example_latest_only.py
airflow/example_dags/example_latest_only_with_trigger.py
airflow/example_dags/example_passing_params_via_test_command.py
airflow/example_dags/example_python_operator.py
airflow/example_dags/example_short_circuit_operator.py
airflow/example_dags/example_skip_dag.py
airflow/example_dags/example_subdag_operator.py
airflow/example_dags/example_trigger_controller_dag.py
airflow/example_dags/example_trigger_target_dag.py
airflow/example_dags/example_xcom.py
airflow/example_dags/test_utils.py
airflow/example_dags/tutorial.py
airflow/example_dags/subdags/__init__.py
airflow/example_dags/subdags/subdag.py
airflow/executors/__init__.py
airflow/executors/base_executor.py
airflow/executors/celery_executor.py
airflow/executors/dask_executor.py
airflow/executors/local_executor.py
airflow/executors/sequential_executor.py
airflow/hooks/S3_hook.py
airflow/hooks/__init__.py
airflow/hooks/base_hook.py
airflow/hooks/dbapi_hook.py
airflow/hooks/docker_hook.py
airflow/hooks/druid_hook.py
airflow/hooks/hdfs_hook.py
airflow/hooks/hive_hooks.py
airflow/hooks/http_hook.py
airflow/hooks/jdbc_hook.py
airflow/hooks/mssql_hook.py
airflow/hooks/mysql_hook.py
airflow/hooks/oracle_hook.py
airflow/hooks/pig_hook.py
airflow/hooks/postgres_hook.py
airflow/hooks/presto_hook.py
airflow/hooks/samba_hook.py
airflow/hooks/sqlite_hook.py
airflow/hooks/webhdfs_hook.py
airflow/hooks/zendesk_hook.py
airflow/macros/__init__.py
airflow/macros/hive.py
airflow/migrations/__init__.py
airflow/migrations/env.py
airflow/migrations/versions/127d2bf2dfa7_add_dag_id_state_index_on_dag_run_table.py
airflow/migrations/versions/13eb55f81627_for_compatibility.py
airflow/migrations/versions/1507a7289a2f_create_is_encrypted.py
airflow/migrations/versions/1968acfc09e3_add_is_encrypted_column_to_variable_.py
airflow/migrations/versions/1b38cef5b76e_add_dagrun.py
airflow/migrations/versions/211e584da130_add_ti_state_index.py
airflow/migrations/versions/2e541a1dcfed_task_duration.py
airflow/migrations/versions/2e82aab8ef20_rename_user_table.py
airflow/migrations/versions/338e90f54d61_more_logging_into_task_isntance.py
airflow/migrations/versions/40e67319e3a9_dagrun_config.py
airflow/migrations/versions/4446e08588_dagrun_start_end.py
airflow/migrations/versions/4addfa1236f1_add_fractional_seconds_to_mysql_tables.py
airflow/migrations/versions/502898887f84_adding_extra_to_log.py
airflow/migrations/versions/52d714495f0_job_id_indices.py
airflow/migrations/versions/561833c1c74b_add_password_column_to_user.py
airflow/migrations/versions/5e7d17757c7a_add_pid_field_to_taskinstance.py
airflow/migrations/versions/64de9cddf6c9_add_task_fails_journal_table.py
airflow/migrations/versions/8504051e801b_xcom_dag_task_indices.py
airflow/migrations/versions/947454bf1dff_add_ti_job_id_index.py
airflow/migrations/versions/__init__.py
airflow/migrations/versions/bba5a7cfc896_add_a_column_to_track_the_encryption_.py
airflow/migrations/versions/bbc73705a13e_add_notification_sent_column_to_sla_miss.py
airflow/migrations/versions/bdaa763e6c56_make_xcom_value_column_a_large_binary.py
airflow/migrations/versions/cc1e65623dc7_add_max_tries_column_to_task_instance.py
airflow/migrations/versions/d2ae31099d61_increase_text_size_for_mysql.py
airflow/migrations/versions/e3a246e0dc1_current_schema.py
airflow/migrations/versions/f2ca10b85618_add_dag_stats_table.py
airflow/operators/__init__.py
airflow/operators/bash_operator.py
airflow/operators/check_operator.py
airflow/operators/dagrun_operator.py
airflow/operators/docker_operator.py
airflow/operators/dummy_operator.py
airflow/operators/email_operator.py
airflow/operators/generic_transfer.py
airflow/operators/hive_operator.py
airflow/operators/hive_stats_operator.py
airflow/operators/hive_to_druid.py
airflow/operators/hive_to_mysql.py
airflow/operators/hive_to_samba_operator.py
airflow/operators/http_operator.py
airflow/operators/jdbc_operator.py
airflow/operators/latest_only_operator.py
airflow/operators/mssql_operator.py
airflow/operators/mssql_to_hive.py
airflow/operators/mysql_operator.py
airflow/operators/mysql_to_hive.py
airflow/operators/oracle_operator.py
airflow/operators/pig_operator.py
airflow/operators/postgres_operator.py
airflow/operators/presto_check_operator.py
airflow/operators/presto_to_mysql.py
airflow/operators/python_operator.py
airflow/operators/redshift_to_s3_operator.py
airflow/operators/s3_file_transform_operator.py
airflow/operators/s3_to_hive_operator.py
airflow/operators/sensors.py
airflow/operators/slack_operator.py
airflow/operators/sqlite_operator.py
airflow/operators/subdag_operator.py
airflow/security/__init__.py
airflow/security/kerberos.py
airflow/security/utils.py
airflow/task_runner/__init__.py
airflow/task_runner/base_task_runner.py
airflow/task_runner/bash_task_runner.py
airflow/ti_deps/__init__.py
airflow/ti_deps/dep_context.py
airflow/ti_deps/deps/__init__.py
airflow/ti_deps/deps/base_ti_dep.py
airflow/ti_deps/deps/dag_ti_slots_available_dep.py
airflow/ti_deps/deps/dag_unpaused_dep.py
airflow/ti_deps/deps/dagrun_exists_dep.py
airflow/ti_deps/deps/exec_date_after_start_date_dep.py
airflow/ti_deps/deps/not_in_retry_period_dep.py
airflow/ti_deps/deps/not_running_dep.py
airflow/ti_deps/deps/not_skipped_dep.py
airflow/ti_deps/deps/prev_dagrun_dep.py
airflow/ti_deps/deps/runnable_exec_date_dep.py
airflow/ti_deps/deps/task_concurrency_dep.py
airflow/ti_deps/deps/trigger_rule_dep.py
airflow/ti_deps/deps/valid_state_dep.py
airflow/utils/__init__.py
airflow/utils/asciiart.py
airflow/utils/compression.py
airflow/utils/dag_processing.py
airflow/utils/dates.py
airflow/utils/db.py
airflow/utils/decorators.py
airflow/utils/email.py
airflow/utils/file.py
airflow/utils/helpers.py
airflow/utils/json.py
airflow/utils/module_loading.py
airflow/utils/operator_helpers.py
airflow/utils/operator_resources.py
airflow/utils/state.py
airflow/utils/tests.py
airflow/utils/timeout.py
airflow/utils/trigger_rule.py
airflow/utils/log/__init__.py
airflow/utils/log/file_processor_handler.py
airflow/utils/log/file_task_handler.py
airflow/utils/log/gcs_task_handler.py
airflow/utils/log/logging_mixin.py
airflow/utils/log/s3_task_handler.py
airflow/www/__init__.py
airflow/www/app.py
airflow/www/blueprints.py
airflow/www/forms.py
airflow/www/gunicorn_config.py
airflow/www/utils.py
airflow/www/validators.py
airflow/www/views.py
airflow/www/api/__init__.py
airflow/www/api/experimental/__init__.py
airflow/www/api/experimental/endpoints.py
airflow/www/static/ace.js
airflow/www/static/airflow.gif
airflow/www/static/bootstrap-theme.css
airflow/www/static/bootstrap-toggle.min.css
airflow/www/static/bootstrap-toggle.min.js
airflow/www/static/bootstrap3-typeahead.min.js
airflow/www/static/connection_form.js
airflow/www/static/d3.tip.v0.6.3.js
airflow/www/static/d3.v3.min.js
airflow/www/static/dagre-d3.js
airflow/www/static/dagre-d3.min.js
airflow/www/static/dagre.css
airflow/www/static/dataTables.bootstrap.css
airflow/www/static/favicon.ico
airflow/www/static/gantt-chart-d3v2.js
airflow/www/static/gantt.css
airflow/www/static/graph.css
airflow/www/static/jqClock.min.js
airflow/www/static/jquery.dataTables.css
airflow/www/static/jquery.dataTables.min.js
airflow/www/static/loading.gif
airflow/www/static/main.css
airflow/www/static/mode-sql.js
airflow/www/static/nv.d3.css
airflow/www/static/nv.d3.js
airflow/www/static/pin.svg
airflow/www/static/pin_100.jpg
airflow/www/static/pin_100.png
airflow/www/static/pin_25.png
airflow/www/static/pin_30.png
airflow/www/static/pin_35.png
airflow/www/static/pin_40.png
airflow/www/static/pin_large.jpg
airflow/www/static/pin_large.png
airflow/www/static/sort_asc.png
airflow/www/static/sort_both.png
airflow/www/static/sort_desc.png
airflow/www/static/theme-crimson_editor.js
airflow/www/static/tree.css
airflow/www/static/underscore.js
airflow/www/static/para/parallel.css
airflow/www/static/para/parallel.js
airflow/www/static/para/webgl-2d.js
airflow/www/static/screenshots/gantt.png
airflow/www/static/screenshots/graph.png
airflow/www/static/screenshots/tree.png
airflow/www/templates/admin/master.html
airflow/www/templates/airflow/chart.html
airflow/www/templates/airflow/circles.html
airflow/www/templates/airflow/code.html
airflow/www/templates/airflow/config.html
airflow/www/templates/airflow/confirm.html
airflow/www/templates/airflow/conn_create.html
airflow/www/templates/airflow/conn_edit.html
airflow/www/templates/airflow/conn_list.html
airflow/www/templates/airflow/dag.html
airflow/www/templates/airflow/dag_code.html
airflow/www/templates/airflow/dag_details.html
airflow/www/templates/airflow/dags.html
airflow/www/templates/airflow/duration_chart.html
airflow/www/templates/airflow/gantt.html
airflow/www/templates/airflow/graph.html
airflow/www/templates/airflow/list_dags.html
airflow/www/templates/airflow/login.html
airflow/www/templates/airflow/master.html
airflow/www/templates/airflow/model_create.html
airflow/www/templates/airflow/model_edit.html
airflow/www/templates/airflow/model_list.html
airflow/www/templates/airflow/modelchart.html
airflow/www/templates/airflow/noaccess.html
airflow/www/templates/airflow/nvd3.html
airflow/www/templates/airflow/query.html
airflow/www/templates/airflow/task.html
airflow/www/templates/airflow/task_instance.html
airflow/www/templates/airflow/ti_code.html
airflow/www/templates/airflow/ti_log.html
airflow/www/templates/airflow/traceback.html
airflow/www/templates/airflow/tree.html
airflow/www/templates/airflow/variable_list.html
airflow/www/templates/airflow/version.html
airflow/www/templates/airflow/xcom.html
airflow/www/templates/airflow/chart/create.html
airflow/www/templates/airflow/chart/edit.html
airflow/www/templates/airflow/para/para.html
airflow/www/templates/airflow/variables/README.md
apache_airflow.egg-info/PKG-INFO
apache_airflow.egg-info/SOURCES.txt
apache_airflow.egg-info/dependency_links.txt
apache_airflow.egg-info/not-zip-safe
apache_airflow.egg-info/requires.txt
apache_airflow.egg-info/top_level.txt
scripts/systemd/README
scripts/systemd/airflow
scripts/systemd/airflow-flower.service
scripts/systemd/airflow-kerberos.service
scripts/systemd/airflow-scheduler.service
scripts/systemd/airflow-webserver.service
scripts/systemd/airflow-worker.service
scripts/systemd/airflow.conf
scripts/upstart/README
scripts/upstart/airflow-flower.conf
scripts/upstart/airflow-scheduler.conf
scripts/upstart/airflow-webserver.conf
scripts/upstart/airflow-worker.conf
tests/__init__.py
tests/configuration.py
tests/core.py
tests/impersonation.py
tests/jobs.py
tests/models.py
tests/plugins_manager.py
tests/utils.py
tests/api/__init__.py
tests/api/client/__init__.py
tests/api/common/__init__.py
tests/contrib/__init__.py
tests/contrib/hooks/__init__.py
tests/contrib/hooks/test_ftp_hook.py
tests/contrib/hooks/test_jira_hook.py
tests/contrib/operators/__init__.py
tests/contrib/sensors/__init__.py
tests/executors/__init__.py
tests/executors/test_executor.py
tests/operators/__init__.py
tests/operators/docker_operator.py
tests/operators/hive_operator.py
tests/operators/latest_only_operator.py
tests/operators/operators.py
tests/operators/python_operator.py
tests/operators/s3_to_hive_operator.py
tests/operators/sensors.py
tests/operators/subdag_operator.py
tests/test_utils/__init__.py
tests/test_utils/fake_datetime.py
tests/ti_deps/__init__.py
tests/ti_deps/contexts/__init__.py
tests/ti_deps/deps/__init__.py
tests/ti_deps/deps/fake_models.py
tests/ti_deps/deps/test_dag_ti_slots_available_dep.py
tests/ti_deps/deps/test_dag_unpaused_dep.py
tests/ti_deps/deps/test_dagrun_exists_dep.py
tests/ti_deps/deps/test_not_in_retry_period_dep.py
tests/ti_deps/deps/test_not_running_dep.py
tests/ti_deps/deps/test_not_skipped_dep.py
tests/ti_deps/deps/test_prev_dagrun_dep.py
tests/ti_deps/deps/test_runnable_exec_date_dep.py
tests/ti_deps/deps/test_trigger_rule_dep.py
tests/ti_deps/deps/test_valid_state_dep.py
tests/utils/__init__.py
tests/www/__init__.py
tests/www/api/__init__.py
tests/www/api/experimental/__init__.py
tests/www/api/experimental/test_endpoints.py
tests/www/api/experimental/test_kerberos_endpoints.py