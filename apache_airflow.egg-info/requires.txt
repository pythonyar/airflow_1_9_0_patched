alembic<0.9,>=0.8.3
bleach==2.1.2
configparser<3.6.0,>=3.5.0
croniter<0.4,>=0.3.17
dill<0.3,>=0.2.2
flask<0.12,>=0.11
flask-admin==1.4.1
flask-cache<0.14,>=0.13.1
flask-login==0.2.11
flask-swagger==0.2.13
flask-wtf==0.14
funcsigs==1.0.0
future<0.17,>=0.16.0
gitpython>=2.0.2
gunicorn<20.0,>=19.4.0
jinja2<2.9.0,>=2.7.3
lxml<4.0,>=3.6.0
markdown<3.0,>=2.5.2
pandas<1.0.0,>=0.17.1
psutil<5.0.0,>=4.2.0
pygments<3.0,>=2.0.1
python-daemon<2.2,>=2.1.1
python-dateutil<3,>=2.3
python-nvd3==0.14.2
requests<3,>=2.5.1
setproctitle<2,>=1.1.8
sqlalchemy>=0.9.8
tabulate<0.8.0,>=0.7.5
thrift>=0.9.2
zope.deprecation<5.0,>=4.0

[all]
click
freezegun
jira
lxml>=3.3.4
mock
moto==1.1.19
nose
nose-ignore-docstring==0.2
nose-timer
parameterized
rednose
paramiko
requests_mock
psycopg2>=2.7.1
mysqlclient>=1.3.6
hive-thrift-py>=0.0.1
pyhive>=0.1.3
impyla>=0.13.3
unicodecsv>=0.14.1
pymssql>=2.1.1
unicodecsv>=0.14.1
snakebite>=2.7.8
vertica-python>=0.5.1
cloudant<2.0,>=0.5.9
sphinx>=1.2.3
sphinx-argparse>=0.1.13
sphinx-rtd-theme>=0.1.6
Sphinx-PyPI-upload>=0.2.1
pysmbclient>=0.1.3
boto>=2.36.0
filechunkio>=1.6
slackclient>=1.0.0
cryptography>=0.9.3
cx_Oracle>=5.1.2
docker-py>=1.6.0
paramiko>=2.1.1

[all_dbs]
psycopg2>=2.7.1
mysqlclient>=1.3.6
hive-thrift-py>=0.0.1
pyhive>=0.1.3
impyla>=0.13.3
unicodecsv>=0.14.1
pymssql>=2.1.1
unicodecsv>=0.14.1
snakebite>=2.7.8
vertica-python>=0.5.1
cloudant<2.0,>=0.5.9

[async]
greenlet>=0.4.9
eventlet>=0.9.7
gevent>=0.13

[azure]
azure-storage>=0.34.0

[celery]
celery>=4.0.0
flower>=0.7.3

[cgroups]
cgroupspy>=0.1.4

[cloudant]
cloudant<2.0,>=0.5.9

[crypto]
cryptography>=0.9.3

[dask]
distributed<2,>=1.15.2

[databricks]
requests<3,>=2.5.1

[datadog]
datadog>=0.14.0

[devel]
click
freezegun
jira
lxml>=3.3.4
mock
moto==1.1.19
nose
nose-ignore-docstring==0.2
nose-timer
parameterized
rednose
paramiko
requests_mock
mysqlclient>=1.3.6
sphinx>=1.2.3
sphinx-argparse>=0.1.13
sphinx-rtd-theme>=0.1.6
Sphinx-PyPI-upload>=0.2.1
bcrypt>=2.0.0
flask-bcrypt>=0.7.1
boto>=2.36.0
filechunkio>=1.6
cgroupspy>=0.1.4

[devel_hadoop]
click
freezegun
jira
lxml>=3.3.4
mock
moto==1.1.19
nose
nose-ignore-docstring==0.2
nose-timer
parameterized
rednose
paramiko
requests_mock
mysqlclient>=1.3.6
sphinx>=1.2.3
sphinx-argparse>=0.1.13
sphinx-rtd-theme>=0.1.6
Sphinx-PyPI-upload>=0.2.1
bcrypt>=2.0.0
flask-bcrypt>=0.7.1
boto>=2.36.0
filechunkio>=1.6
cgroupspy>=0.1.4
hive-thrift-py>=0.0.1
pyhive>=0.1.3
impyla>=0.13.3
unicodecsv>=0.14.1
snakebite>=2.7.8
hdfs[avro,dataframe,kerberos]>=2.0.4
pykerberos>=1.1.13
requests_kerberos>=0.10.0
thrift_sasl>=0.2.0
snakebite[kerberos]>=2.7.8
kerberos>=1.2.5

[doc]
sphinx>=1.2.3
sphinx-argparse>=0.1.13
sphinx-rtd-theme>=0.1.6
Sphinx-PyPI-upload>=0.2.1

[docker]
docker-py>=1.6.0

[emr]
boto3>=1.0.0

[gcp_api]
httplib2
google-api-python-client<1.6.0,>=1.5.0
oauth2client<2.1.0,>=2.0.2
PyOpenSSL
google-cloud-dataflow
pandas-gbq

[github_enterprise]
Flask-OAuthlib>=0.9.1

[hdfs]
snakebite>=2.7.8

[hive]
hive-thrift-py>=0.0.1
pyhive>=0.1.3
impyla>=0.13.3
unicodecsv>=0.14.1

[jdbc]
jaydebeapi>=1.1.1

[jira]
JIRA>1.0.7

[kerberos]
pykerberos>=1.1.13
requests_kerberos>=0.10.0
thrift_sasl>=0.2.0
snakebite[kerberos]>=2.7.8
kerberos>=1.2.5

[ldap]
ldap3>=0.9.9.1

[mssql]
pymssql>=2.1.1
unicodecsv>=0.14.1

[mysql]
mysqlclient>=1.3.6

[oracle]
cx_Oracle>=5.1.2

[password]
bcrypt>=2.0.0
flask-bcrypt>=0.7.1

[postgres]
psycopg2>=2.7.1

[qds]
qds-sdk>=1.9.6

[rabbitmq]
librabbitmq>=1.6.1

[redis]
redis>=2.10.5

[s3]
boto>=2.36.0
filechunkio>=1.6

[salesforce]
simple-salesforce>=0.72

[samba]
pysmbclient>=0.1.3

[slack]
slackclient>=1.0.0

[ssh]
paramiko>=2.1.1

[statsd]
statsd<4.0,>=3.0.1

[vertica]
vertica-python>=0.5.1

[webhdfs]
hdfs[avro,dataframe,kerberos]>=2.0.4
